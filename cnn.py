from keras.models import Sequential
from keras.layers import Dense, Conv2D, MaxPooling2D, Dropout, Flatten
from keras.utils import to_categorical

import leaf_classification
import numpy as np


def createModel(n_classes):
    
    model = Sequential()
    #Adding 2 Convolutional Layers (32 filters 3x3), 1 Max Pooling Layer and 1 Dropout of 25%
    model.add(Conv2D(32, (3, 3), padding='same', activation='relu', input_shape=(32,32,3)))
    model.add(Conv2D(32, (3, 3), activation='relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(0.25))
    
    #Adding 2 Convolutional Layers (64 filters 3x3), 1 Max Pooling Layer and 1 Dropout of 25%	
    model.add(Conv2D(64, (3, 3), padding='same', activation='relu'))
    model.add(Conv2D(64, (3, 3), activation='relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(0.25))
    
    #Adding 2 Convolutional Layers (64 filters 3x3), 1 Max Pooling Layer and 1 Dropout of 25%
    model.add(Conv2D(64, (3, 3), padding='same', activation='relu'))
    model.add(Conv2D(64, (3, 3), activation='relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(0.25))
    
    #Adding a Dense Layer with 512 neurons, Dropout of 50% and dense layer with 2 outputs
    model.add(Flatten())
    model.add(Dense(512, activation='relu'))
    model.add(Dropout(0.5))
    model.add(Dense(n_classes, activation='softmax'))
     
    return model


print "Getting the data"

images = leaf_classification.get_images(training=True)

train_data = []
train_labels = []
species_count = 0

print "Done"
n_classes = len(images)
print "Number of classes: ", n_classes

#Get the training data (The labels are from 0 to n, where n is the number of species)
for specie in images:
	for image in images[specie]:
		train_labels.append(species_count)
		train_data.append(image)	
	species_count += 1

#Convert to numpy arrays
train_data = np.array(train_data) 
train_labels = np.array(train_labels)

train_labels = to_categorical(train_labels) #Convert to categorical variables

#Creates the model
model = createModel(n_classes)
batch_size = 32	#Size of the batch
epochs = 30	#Number of epochs in training
model.compile(optimizer='rmsprop', loss='categorical_crossentropy', metrics=['accuracy'])
history = model.fit(train_data, train_labels, batch_size=batch_size, epochs=epochs, verbose=1)


#------------------ Testing ----------------------------
#Get the test data
images = leaf_classification.get_images(training=False)

test_data = []
test_labels = []
species_count = 0

n_classes = len(images)
for specie in images:
	for image in images[specie]:
		test_labels.append(species_count)
		test_data.append(image)	
	species_count += 1


test_data = np.array(test_data)
test_labels = np.array(test_labels)

test_labels = to_categorical(test_labels)

score = model.evaluate(test_data, test_labels) #Evaluate results for test data
print('Test loss:', score[0])
print('Test accuracy:', score[1])
