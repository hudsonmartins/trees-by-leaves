This is the project for the course MO810 - Introduction to Deep Learning at University of Campinas. The goal of this project is to classify trees by its leaves using Convolutional Neural Networks.
The dataset for this project is taken from http://leafsnap.com/dataset/

* At first, run the split_dataset code. This script will split the dataset (from lab images) into training and test, respecting the proportion 80% for training and 20% for test
