from keras.models import Sequential
from keras.layers import Dense, Conv2D, MaxPooling2D, Dropout, Flatten, BatchNormalization
from keras.utils import to_categorical

import leaf_classification
import numpy as np


def createModel(n_classes):
    '''
    #--------------------------------------------------------------------------------------------#
    |	Model described on Plant species classification using deep convolutional neural network  |
    |   by Dyrmann, M.; Karstoft, H. and Midtiby H. S. (2016) 					 |
    #--------------------------------------------------------------------------------------------#
    '''
    
    model = Sequential()
    model.add(Conv2D(64, (5, 5), padding='same', activation='relu', strides = 1, input_shape=(64,64,3)))
    model.add(BatchNormalization(axis=1, momentum=0.99, epsilon=0.001))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    
    model.add(Conv2D(32, (1, 1), padding='same', activation='relu', strides = 1))
    model.add(Conv2D(32, (2, 2), padding='same', activation='relu', strides = 1)) 
    model.add(Conv2D(32, (2, 2), padding='same', activation='relu', strides = 1))
    model.add(BatchNormalization(axis=1, momentum=0.99, epsilon=0.001))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    
    model.add(Conv2D(32, (5, 5), padding='same', activation='relu', strides = 1))
    model.add(Conv2D(32, (5, 5), padding='same', activation='relu', strides = 1))
    model.add(BatchNormalization(axis=1, momentum=0.99, epsilon=0.001))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    
    model.add(Conv2D(32, (5, 5), padding='same', activation='relu', strides = 1))
    model.add(BatchNormalization(axis=1, momentum=0.99, epsilon=0.001))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(0.5))
        
    model.add(Flatten())
    model.add(Dense(512, activation='relu'))
    model.add(Dropout(0.5))
    model.add(Dense(n_classes, activation='softmax'))
     
    return model


print "Getting the data"

images = leaf_classification.get_images(training=True)

train_data = []
train_labels = []
species_count = 0

print "Done"
n_classes = len(images)
print "Number of classes: ", n_classes

#Get the training data (The labels are from 0 to n, where n is the number of species)
for specie in images:
	for image in images[specie]:
		train_labels.append(species_count)
		train_data.append(image)	
	species_count += 1

#Convert to numpy arrays
train_data = np.array(train_data) 
train_labels = np.array(train_labels)

train_labels = to_categorical(train_labels) #Convert to categorical variables

#Creates the model
model = createModel(n_classes)
batch_size = 32 #Size of the batch
epochs = 50	#Number of epochs in training
model.compile(optimizer='rmsprop', loss='categorical_crossentropy', metrics=['accuracy'])
history = model.fit(train_data, train_labels, batch_size=batch_size, epochs=epochs, verbose=1)


#------------------ Testing ----------------------------
#Get the test data
images = leaf_classification.get_images(training=False)

test_data = []
test_labels = []
species_count = 0

n_classes = len(images)
for specie in images:
	for image in images[specie]:
		test_labels.append(species_count)
		test_data.append(image)	
	species_count += 1


test_data = np.array(test_data)
test_labels = np.array(test_labels)

test_labels = to_categorical(test_labels)

score = model.evaluate(test_data, test_labels) #Evaluate results for test data
print('Test loss:', score[0])
print('Test accuracy:', score[1])
