from PIL import Image
import numpy as np
import glob, os.path


def get_images(training):
	"""
		Crop the images for all given classes
		Returns a Dictionary with images and the specie as the key for each class of images
	"""
	classes = os.listdir('dataset/images/lab')

	img_dict = dict()
		
	for specie in classes:
		if training:
			path = 'dataset/images/lab/'+specie	
		else:
			path = 'dataset/images/lab/'+specie+'/test'
			
		for image in glob.glob(path+'/*.jpg'): 
			img = Image.open(image)     #Open the image
			center = [img.size[0]/2, img.size[1]/2]   #Gets the center of the image
			img = img.crop((center[0]-250, center[1]-250, center[0]+250, center[1]+250))  #Crops the image from the center to 500x500 pixels
			img = img.resize((32, 32))

			#img.show()
			img_array = np.asarray(img)/255.0 #Convert to np array
			img.close()
						
			if(specie in img_dict):  #save in the dictionary
				img_dict[specie].append(img_array)
			else:
				img_dict[specie] = [img_array]
	
	return img_dict


